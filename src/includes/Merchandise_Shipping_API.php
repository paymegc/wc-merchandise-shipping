<?php

class MerchandiseShippingAPI
{
    const SHIPPING_SERVICE = "https://api.courier-services.us/";

    public function request($url, $method = "GET", $data = array()) {
        // set request parameters (headers, body)
        $request = array(
            "timeout" => 60,
            "headers" => array(
                // "Authorization" => "Basic " . base64_encode("{$this->user_key}:{$this->password_key}"),
                "cache-control" => "no-cache",
                "content-type" => "application/json"),
        );

        if ($this->debug) {
            // loggerMerchandiseShipping(">> Request ");
            // loggerMerchandiseShipping($request);
        }

        // send request
        if ($method == "POST") {
            $request["body"] = json_encode($data);
            $response = wp_safe_remote_post($url, $request);
        } else {
            $dataQuery = http_build_query( $data);
            $url .=  "?{$dataQuery}";
            $response = wp_safe_remote_get($url, $request);
        }
        $res = wp_remote_retrieve_body($response);

        if ($response["status_code"] != "200") {
            loggerMerchandiseShipping(">> Request URL: " . $url);
            loggerMerchandiseShipping($response);
            loggerMerchandiseShipping($res);
        }

        return json_decode($res, true);
    }
}
