<?php

class GatewayShippingPlugin
{

    public $plugin_name;
    private static $initiated = false;

    public function __construct($file)
    {
        if (!$this->validate_requirements()) {
            return;
        }

        $this->plugin_name = plugin_basename($file);
        
    }

    public function init()
    {
        if (self::$initiated) {
            return;
        } else {
            self::$initiated = true;
        }

        require_once WC_MERCHANDISE_SHIPPING_DIR . 'src/includes/WC_Merchandise_Shipping_Method.php';
        // add plugin settings button
        add_filter('plugin_action_links_' . $this->plugin_name, array('GatewayShippingPlugin', 'set_action_links'));
        add_filter('woocommerce_shipping_methods', array('GatewayShippingPlugin', 'add_merchandise_shipping_method'));
        add_action('woocommerce_order_status_changed', array(new WCMerchandiseShippingMethod(), 'generate_guide'), 20, 4);

        // if WC-vendor plugin is active, add WC-vendor shipping type
        if (in_array('wc-vendors-pro/wcvendors-pro.php', apply_filters('active_plugins', get_option('active_plugins')))) {
            add_filter('wcv_custom_user_shiping_fields', array('GatewayShippingPlugin', 'add_wc_vendors_shipping_type'), 10, 1);
        }
    }

    /**
     * Attached to activate_{ plugin_basename( __FILES__ ) } by register_activation_hook()
     * @static
     */
    public static function activate()
    {
        flush_rewrite_rules();
    }

    /**
     * Attached to deactivate_{ plugin_basename( __FILES__ ) } by register_deactivation_hook()
     * @static
     */
    public static function deactivate()
    {
        // flush rewrite rules
        flush_rewrite_rules();

    }

    /**
     * Attached to uninstall plugin
     * @static
     */
    public static function uninstall()
    {
    }

    /**
     * setup plugin action links
     */
    public static function set_action_links($links)
    {
        $plugin_links = array();
        // settings link
        $plugin_links[] = '<a href="' . admin_url('admin.php?page=wc-settings&tab=shipping&section=wc_merchandise_shipping') . '">' . 'Settings' . '</a>';

        return array_merge($plugin_links, $links);
    }

    /**
     * Display an admin notice
     */
    public static function admin_notice($notice, $type)
    {
        $class = "notice-info";
        if ($type == "error") {
            $class = "notice-error";
        } elseif ($type == "warning") {
            $class = "notice-warning";
        } elseif ($type == "success") {
            $class = "notice-success";
        }

        echo "<div class='notice $class'>" .
        '<p>' . esc_html($notice) . '</p>' .
            '</div>';
    }

    public static function log($msg)
    {
        if (is_array($msg) || is_object($msg)) {
            $msg = print_r($msg, true);
        }

        $logger = new WC_Logger();
        $logger->add('wc-merchandise-shipping-plugin', $msg);
    }

    public static function validate_requirements()
    {
        if (version_compare(PHP_VERSION, '7.1.0', '<')) {
            if (is_admin() && !defined('DOING_AJAX')) {
                add_action(
                    'admin_notices',
                    function () {
                        GatewayShippingPlugin::admin_notice('Woocommerce Gateway Shipping Plugin: plugin fue desarrollado usando PHP 7, algunas funcionalidades podrían fallar en esta versión', 'warning');
                    }
                );
            }
        }

        if (!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')), true)) {
            if (is_admin() && !defined('DOING_AJAX')) {
                add_action(
                    'admin_notices',
                    function () {
                        GatewayShippingPlugin::admin_notice('Woocommerce Gateway Shipping Plugin: Requiere que se encuentre instalado y activo el plugin: Woocommerce', 'error');
                    }
                );
            }
            return false;
        }

        return true;
    }

    public static function add_merchandise_shipping_method($methods)
    {
        $methods['wc_merchandise_shipping'] = 'WCMerchandiseShippingMethod';
        return $methods;
    }

    public static function add_wc_vendors_shipping_type($params)
    {
        // save original shipping types
        $shipping_types = $params['shipping_general']['fields']['_wcv_shipping_type']['options'];

        // set custom shipping type
        $custom_shipping = array('gateway_shipping' => 'Gateway Shipping');

        // add new shipping type
        $params['shipping_general']['fields']['_wcv_shipping_type']['options'] = array_merge($shipping_types, $custom_shipping);

        return $params;
    }
}
