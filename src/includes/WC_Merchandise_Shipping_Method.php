<?php

class WCMerchandiseShippingMethod extends WC_Shipping_Method
{
    const SETTINGS_KEY = 'wc_merchandise_shipping_settings';
    const POSTMETA_GUIDE = '_shipping_guide';

    public function __construct($instance_id = 0)
    {
        $this->id                 = 'wc_merchandise_shipping';
        $this->instance_id        = absint($instance_id);
        $this->method_title       = __('Merchandise Shipping');
        $this->method_description = __('Custom store shipping');
        $this->enabled = isset($this->settings['enabled']) ? $this->settings['enabled'] : 'yes';
        $this->title = isset($this->settings['title']) ? $this->settings['title'] : __('Merchandise Shipping');

        $this->debug = true;

        $settings = get_option(self::SETTINGS_KEY);

        $this->courier_profile = isset($settings["courier_profile"]) ? $settings["courier_profile"] : "";

        // $this->availability = 'including';

        $this->supports = array(
            'settings',
            'shipping-zones',
            'instance-settings',
        );

        $this->init_form_fields();
    }

    /**
     * Define settings field for this shipping
     * @return void 
     */
    public function init_form_fields()
    {
        $this->form_fields = array(
            'merchandise_settings' => array(
                'type' => 'merchandise_shipping_settings',
            )
        );
    }

    /**
     * Load settings HTML for this shipping
     * @return void 
     */
    public function generate_merchandise_shipping_settings_html()
    {
        include(WC_MERCHANDISE_SHIPPING_DIR . 'src/templates/settings.php');
    }

    /**
     * This function is used to calculate the shipping cost. Within this function we can check for weights, dimensions and other parameters.
     *
     * @access public
     * @param mixed $package
     * @return void
     */
    public function calculate_shipping($package = array())
    {
        require_once WC_MERCHANDISE_SHIPPING_DIR . 'src/includes/Merchandise_Shipping_API.php';

        if ($this->debug) loggerMerchandiseShipping("################ START CALCULATE SHIPPING ################");

        $customer = WC()->customer;

        $shippingCountry  = $package["destination"]["country"];
        $shippingRegion   = $package['destination']['state'];
        $shippingCity     = $package['destination']['city'];
        $shippingPostCode = $package['destination']['postcode'];

        // get shipping info
        $data = array(
            "profile" => $this->courier_profile,
            "firstName" => $customer->get_billing_first_name(),
            "lastName" => $customer->get_billing_last_name(),
            "country" => $shippingCountry,
            "address" => $customer->get_billing_address_1() . ", " . $customer->get_billing_address_2(),
            "city" => $shippingCity,
            "state" => $shippingRegion,
            "postalCode" => $shippingPostCode,
            "email" => $customer->get_billing_email(),
            "phone" => $customer->get_billing_phone(),
            "amount" => $order_total,
        );
        
        loggerMerchandiseShipping($data);
        $api = new MerchandiseShippingAPI();
        $shipping_info = $api->request(MerchandiseShippingAPI::SHIPPING_SERVICE, "POST", $data);

        $shippingTax = $shipping_info["data"]["shippingTax"];
        $trackingCode = $shipping_info["data"]["trackingCode"];
        
        // check shipping results
        // if (!isset($shipping_info["status"]) || $shipping_info["status"] != "success") {
        //     return array(
        //         "status" => "fail",
        //         "data" => array(
        //             "shippingTax" => "",
        //             "shippingResult" => $shipping_info,
        //             // "data" => $data
        //         ),
        //     );
        // }

        
        // build rate
        $rate = array(
            'id' => "{$this->id}-{$this->instance_id}-{$vendorId}-{$courrierId}",
            'label' => "Custom Store Shipping",
            'cost' => 100,
            'package' => $package,
            // 'meta_data' => array(
            //     'courrier_id' => $courrierId,
            //     'vendor_id' => $vendorId
            // )
        );

        
        // Register the rate
        $this->add_rate($rate);

        if ($this->debug) loggerMerchandiseShipping("################ END CALCULATE SHIPPING ################");
    }

    public function generate_guide($orderId, $oldStatus, $newStatus, WC_Order $order)
    {
        require_once WC_MERCHANDISE_SHIPPING_DIR . 'src/includes/Merchandise_Shipping_API.php';

        if ($this->debug) loggerMerchandiseShipping  ("################ START GENERATE GUIDE ################");

        $existingGuide = get_post_meta($orderId, self::POSTMETA_GUIDE, true);
        
        // TODO: improve validation
        if (empty($existingGuide) && 'processing' === $newStatus) {
        // if ($order->has_shipping_method($this->id) && empty($existingGuide) && 'processing' === $newStatus) {
            // Get coordinadora settings
            $pluginSettings = get_option(self::SETTINGS_KEY);

            if ($order->get_shipping_first_name()) {
                $customerFirstName = $order->get_shipping_first_name();
                $customerLastName = $order->get_shipping_last_name();
            } else {
                $customerFirstName = $order->get_billing_first_name();
                $customerLastName = $order->get_billing_last_name();
            }

            if ($order->get_shipping_address_1()) {
                $shippingAddress = sprintf(
                    "%s %s",
                    $order->get_shipping_address_1(),
                    $order->get_shipping_address_2()
                );
            } else {
                $shippingAddress = sprintf(
                    "%s %s",
                    $order->get_billing_address_1(),
                    $order->get_billing_address_2()
                );
            }

            $shippingCountry  = $order->get_shipping_country() ? $order->get_shipping_country() : $order->get_billing_country();
            $shippingRegion   = $order->get_shipping_state() ? $order->get_shipping_state() : $order->get_billing_state();
            $shippingCity     = $order->get_shipping_city() ? $order->get_shipping_city() : $order->get_billing_city();
            $shippingPostCode = $order->get_shipping_postcode() ? $order->get_shipping_postcode() : $order->get_billing_postcode();
        
            // get shipping info
            $data = array(
                "profile" => $pluginSettings["courier_profile"],
                "firstName" => $customerFirstName,
                "lastName" => $customerLastName,
                "country" => $shippingCountry,
                "address" => $shippingAddress,
                "city" => $shippingCity,
                "state" => $shippingRegion,
                "postalCode" => $shippingPostCode,
                "email" => $order->get_billing_email(),
                "phone" =>  $order->get_billing_phone(),
                "amount" => $order->get_total(),
            );

            loggerMerchandiseShipping($data);
            $api = new MerchandiseShippingAPI();
            $shipping_info = $api->request(MerchandiseShippingAPI::SHIPPING_SERVICE, "POST", $data);

            $shippingTax = $shipping_info["data"]["shippingTax"];
            $trackingCode = $shipping_info["data"]["trackingCode"];
                
            if (!$trackingCode) {
                return;
            }

            $orderNote = __( 'Tracking code: '.$trackingCode);
            update_post_meta($order->get_id(), self::POSTMETA_GUIDE, $trackingCode);
            $order->add_order_note($orderNote);
        }

        if ($this->debug) loggerMerchandiseShipping("################ END GENERATE GUIDE ################");
    }
}

