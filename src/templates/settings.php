<?php

if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

$shippingSettings = get_option(WCMerchandiseShippingMethod::SETTINGS_KEY);
$shippingSettings = empty($shippingSettings) ? array() : $shippingSettings;

if (isset($_POST["save"])) {
    if (!isset($_POST["wc_merchandise_shipping_nonce"]) || !wp_verify_nonce($_POST['wc_merchandise_shipping_nonce'], 'wc_merchandise_shipping_nonce')) {
        return;
    }

    // authentication settings
    $shippingSettings["courier_profile"] = sanitize_text_field($_POST["courier_profile"]);

    if (WCMerchandiseShippingMethod::SETTINGS_KEY != null) {
        update_option(WCMerchandiseShippingMethod::SETTINGS_KEY, $shippingSettings);
    }

}

?>
<div>
    <h2>General:</h2>
    <!-- <p>Description</p> -->
</div>
<table class="form-table"><tbody>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="courier_profile">Courier Profile <span class="woocommerce-help-tip"></span></label>
        </th>
        <td class="forminp forminp-text">
            <input name="courier_profile" id="courier_profile" type="text" style="" value="<?=(isset($shippingSettings["courier_profile"]) ? $shippingSettings["courier_profile"] : "")?>" class="input-text regular-input" placeholder="">
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align:center;">
            <?=wp_nonce_field("wc_merchandise_shipping_nonce", "wc_merchandise_shipping_nonce")?>
        </td>
    </tr>
</tbody></table>