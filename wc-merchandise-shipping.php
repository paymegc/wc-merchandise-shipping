<?php
/**
 * @package WoocommerceMerchandiseShipping
 */
/**
 * Plugin Name: Paymegc Merchandise Shipping
 * Plugin URI:  
 * Description: Shipping plugin for Woocomerce Merchandise Gateway
 * Author:      
 * Author URI:  
 * Developer: 
    Version: 1.21.9.21.3
 * Text Domain: wc-gateway-shipping
 *
 */
/*
License...

Copyright 2020 Startsco, Inc.
 */
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/paymegc/wc-merchandise-shipping',
	__FILE__,
	'wc-merchandise-shipping'
);

//Optional: If you're using a private repository, specify the access token like this:
$myUpdateChecker->setAuthentication('MqiAAZzo9WNBAGzgwc3L');

//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('master');

if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

define('WC_MERCHANDISE_SHIPPING_DIR', plugin_dir_path(__FILE__));

require_once plugin_dir_path(__FILE__) . 'src/includes/WC_Gateway_Plugin.php';

// activation
register_activation_hook(__FILE__, array('GatewayShippingPlugin', 'activate'));
// deactivation
register_deactivation_hook(__FILE__, array('GatewayShippingPlugin', 'deactivate'));
// unisnstall
register_uninstall_hook(__FILE__, array('GatewayShippingPlugin', 'uninstall'));

// initialize plugin
add_action('init', array(new GatewayShippingPlugin(__FILE__), 'init'));

if (!function_exists('loggerMerchandiseShipping')) {
    function loggerMerchandiseShipping($log) {GatewayShippingPlugin::log($log);}
}